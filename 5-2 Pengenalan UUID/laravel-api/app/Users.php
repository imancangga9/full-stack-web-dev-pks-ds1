<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = ['id' , 'username', 'email', 'name', 'roles_id'];

    protected $keyType = 'string' ;

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function ($model){
            if(empty( $model->id) ){
                $model->id = Str::uuid();
            }
        });
    }

    public function roles(){
        return $this->belongsToMany('App\Roles');
    }
}
